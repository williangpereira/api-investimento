package br.com.carteiraInvestimento.services;


import br.com.carteiraInvestimento.models.Investimento;
import br.com.carteiraInvestimento.models.Simulacao;
import br.com.carteiraInvestimento.models.SimulacaoResultado;
import br.com.carteiraInvestimento.repositories.SimulacaoRepository;
import org.springframework.beans.factory.ObjectProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.crypto.spec.IvParameterSpec;
import java.util.Optional;

@Service
public class SimulacaoService {

    @Autowired
    private SimulacaoRepository simulacaoRepository;

    @Autowired
    private InvestimentoService investimentoService;


    public Iterable<Simulacao> buscarSimulacoes(){
        Iterable<Simulacao> simulacoesRetornadas = simulacaoRepository.findAll();
        return simulacoesRetornadas;
    }

    public SimulacaoResultado criarSimulacao(Simulacao simulacao){
        Investimento investimento = investimentoService.buscarPorID(simulacao.getId_investimento()).get();
        calcularRendimento(simulacao, investimento);

        SimulacaoResultado simulacaoResultado = new SimulacaoResultado();
        simulacaoResultado.setSimulacaoResultado(calcularRendimento(simulacao, investimento));

        return simulacaoResultado;
    }

    public Double calcularRendimento(Simulacao simulacao, Investimento investimento){
        return simulacao.getValorAplicado() * simulacao.getQuantidadeMeses() * investimento.getRendimentoAoMes();
    }


    public Simulacao novaSimulacao(Simulacao simulacao){
        Simulacao novaSimulacao =  simulacaoRepository.save(simulacao);
        return novaSimulacao;
    }
}
