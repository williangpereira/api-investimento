package br.com.carteiraInvestimento.services;

import br.com.carteiraInvestimento.models.Investimento;
import br.com.carteiraInvestimento.models.Simulacao;
import br.com.carteiraInvestimento.repositories.InvestimentoRepository;
import br.com.carteiraInvestimento.repositories.SimulacaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;


@Service
public class InvestimentoService {

    @Autowired
    private InvestimentoRepository investimentoRepository;

    @Autowired
    private SimulacaoRepository simulacaoRepository;

    public Investimento criarInvestimento(Investimento investimento){
        Investimento novoInvestimento = investimentoRepository.save(investimento);
        return novoInvestimento;
    }

    public Iterable<Investimento> mostrarInvestimento(){
        Iterable<Investimento> investimentos = investimentoRepository.findAll();
        return investimentos;
    }


    public Simulacao novaSimulacao(Simulacao simulacao){
        Simulacao novaSimulacao =  simulacaoRepository.save(simulacao);
        return novaSimulacao;
    }


    public Optional<Investimento> buscarPorID(int id){
        return investimentoRepository.findById(id);

    }

}
