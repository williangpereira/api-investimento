package br.com.carteiraInvestimento.dtos;

import br.com.carteiraInvestimento.models.Investimento;

public class RespostaSimulacaoDTO {

     private double redimentoPorMes;
     private double montante;

    public RespostaSimulacaoDTO(double redimentoPorMes, double montante) {
        this.redimentoPorMes = redimentoPorMes;
        this.montante = montante;
    }

    public RespostaSimulacaoDTO(){}


    public double getRedimentoPorMes() {
        return redimentoPorMes;
    }

    public void setRedimentoPorMes(double redimentoPorMes) {
        this.redimentoPorMes = redimentoPorMes;
    }

    public double getMontante() {
        return montante;
    }

    public void setMontante(double montante) {
        this.montante = montante;
    }
}
