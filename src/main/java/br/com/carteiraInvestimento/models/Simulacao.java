package br.com.carteiraInvestimento.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.*;

@Entity
public class Simulacao {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id_investimento;

    private String nomeInteressado;

    @Email(message = "Formato inválido de email")
    @NotNull(message = "O email não poder ser nulo")
    private String email;

    @NotNull
    @NotEmpty
    @DecimalMin(value = "100.00", message = "valor abaixo do mínimo")
    private double valorAplicado;

    @NotNull
    @NotEmpty
    private int quantidadeMeses;

    public Simulacao() {}

    public int getId_investimento() {
        return id_investimento;
    }

    public void setId_investimento(int id_investimento) {
        this.id_investimento = id_investimento;
    }

    public String getNomeInteressado() {
        return nomeInteressado;
    }

    public void setNomeInteressado(String nomeInteressado) {
        this.nomeInteressado = nomeInteressado;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public double getValorAplicado() {
        return valorAplicado;
    }

    public void setValorAplicado(double valorAplicado) {
        this.valorAplicado = valorAplicado;
    }

    public int getQuantidadeMeses() {
        return quantidadeMeses;
    }

    public void setQuantidadeMeses(int quantidadeMeses) {
        this.quantidadeMeses = quantidadeMeses;
    }
}
