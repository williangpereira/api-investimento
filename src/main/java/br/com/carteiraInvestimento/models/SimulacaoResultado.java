package br.com.carteiraInvestimento.models;


public class SimulacaoResultado {

    private double simulacaoResultado;

    public SimulacaoResultado() {}

    public SimulacaoResultado(double simulacaoResultado) {
        this.simulacaoResultado = simulacaoResultado;
    }

    public double getSimulacaoResultado() {
        return simulacaoResultado;
    }

    public void setSimulacaoResultado(double simulacaoResultado) {
        this.simulacaoResultado = simulacaoResultado;
    }
}
