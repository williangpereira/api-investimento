package br.com.carteiraInvestimento.controllers;


import br.com.carteiraInvestimento.models.Investimento;
import br.com.carteiraInvestimento.models.Simulacao;
import br.com.carteiraInvestimento.models.SimulacaoResultado;
import br.com.carteiraInvestimento.services.InvestimentoService;
import br.com.carteiraInvestimento.services.SimulacaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.persistence.criteria.CriteriaBuilder;
import java.util.Optional;

@RestController
@RequestMapping("/simulacao")
public class SimulacaoController {

    @Autowired
    private SimulacaoService simulacaoService;

    private InvestimentoService investimentoService;

    @GetMapping
    public Iterable<Simulacao> buscarSimulacao(){
        Iterable<Simulacao> simulacoesRetornadas = simulacaoService.buscarSimulacoes();
        return simulacoesRetornadas;
    }

}
