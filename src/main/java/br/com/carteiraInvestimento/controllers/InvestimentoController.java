package br.com.carteiraInvestimento.controllers;

import br.com.carteiraInvestimento.models.Investimento;
import br.com.carteiraInvestimento.models.Simulacao;
import br.com.carteiraInvestimento.models.SimulacaoResultado;
import br.com.carteiraInvestimento.services.InvestimentoService;
import br.com.carteiraInvestimento.services.SimulacaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;


@RestController
@RequestMapping("/investimento")
public class InvestimentoController {

    @Autowired
    private InvestimentoService investimentoService;

    @Autowired
    private SimulacaoService simulacaoService;

    @PostMapping
    public Investimento criarInvestimento(@RequestBody Investimento investimento){
        Investimento novoInvestimento = investimentoService.criarInvestimento(investimento);
        return novoInvestimento;
    }

    @GetMapping
    public Iterable<Investimento> mostrarInvestimentos(){
        Iterable<Investimento> listaDeInvestimentos = investimentoService.mostrarInvestimento();
        return listaDeInvestimentos;
    }

    @GetMapping("/{id}")
    public Optional<Investimento> buscarPorID(@PathVariable  int id){
        Optional<Investimento> optional = investimentoService.buscarPorID(id);
        return optional;
    }


    @PostMapping("/{idInvestimento}/simulacao")
    public ResponseEntity<SimulacaoResultado> realizarSimulacao (@RequestBody Simulacao simulacao){
        return ResponseEntity.status(200).body(simulacaoService.criarSimulacao(simulacao));
    }

}
