package br.com.carteiraInvestimento.repositories;


import br.com.carteiraInvestimento.models.Investimento;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface InvestimentoRepository extends CrudRepository<Investimento, Integer> {
}
