package br.com.carteiraInvestimento.repositories;

import br.com.carteiraInvestimento.models.Simulacao;
import org.springframework.data.repository.CrudRepository;

public interface SimulacaoRepository extends CrudRepository<Simulacao, Integer>{

}
